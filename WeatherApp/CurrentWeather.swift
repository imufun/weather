//
//  CurrentWeather.swift
//  WeatherApp
//
//  Created by imran on 9/29/17.
//  Copyright © 2017 homessolutionbd. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeather {
    
    var _CityName : String!
    var _Date : String!
    var _WeatherType : String!
    var _CurrentTemp : Double!
    
    
    var CityName: String {
        if _CityName == nil {
            _CityName = ""
        }
        return _CityName
    }
    
    var Date: String {
        
        if _Date == nil {
            _Date = ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(for: NSData())
        self._Date = "Today \(String(describing: currentDate))"
        return _Date
    }
    var WeatherType: String {
        if _WeatherType == nil {
            _WeatherType = ""
        }
        return _WeatherType
    }
    
    var CurrentTemp: Double {
        if _CurrentTemp == nil {
            _CurrentTemp = 0.0
        }
        return _CurrentTemp
    }
    
    func downloadWeatherDetails(complate: DonwloadComplete){
        let GetCurrentWeatherUrl = URL(string: CURRENT_WEATHER_URL)!
        
        //Alamofire
        
        
        Alamofire.request(GetCurrentWeatherUrl).responseJSON { response in
            
            let result = response.result
            print(response)
            
            if let dict = result.value as? Dictionary<String, AnyObject> {
                
                if let name = dict["name"] as? String {
                    self._CityName = name.capitalized
                    print("City: \(self._CityName)")
                }
                
                if let weather = dict["weather"] as? [Dictionary<String, AnyObject>]{
                    if let main = weather[0]["main"] as? String {
                        self._WeatherType = main.capitalized
                        print("Weateher type: \(self._WeatherType)")
                    }
                }
                
                
                if let main = dict["main"] as? Dictionary <String, AnyObject>{
                    if let CurrentTemp = main["temp"] as? Double {
                        let kelvinToFarenheitDivision = (CurrentTemp * (9/5) - 459.67)
                        let kelvinToFarenheit = Double(round(10 * kelvinToFarenheitDivision/10))
                        self._CurrentTemp = kelvinToFarenheit
                        print("Current Temp: \(self._CurrentTemp)")
                    }
                }
                
            }
            
        }
    }
}


