//
//  Constants.swift
//  WeatherApp
//
//  Created by imran on 9/29/17.
//  Copyright © 2017 homessolutionbd. All rights reserved.
//

import Foundation

let BASE_URL = "http://api.openweathermap.org/data/2.5/weather?"
let LATITUDE = "lat="
let LONGITUDE = "&lon="
let APP_ID = "&appid="
let APP_KEY = "f279df88f01232850ddff621c125603d"

let CURRENT_WEATHER_URL = "\(BASE_URL)\(LATITUDE)23.71\(LONGITUDE)90.41\(APP_ID)\(APP_KEY)"

typealias DonwloadComplete =  () -> ()

//api.openweathermap.org/data/2.5/weather?q=Dhaka,bd&appid=f279df88f01232850ddff621c125603d
