//
//  ViewController.swift
//  WeatherApp
//
//  Created by imran on 9/28/17.
//  Copyright © 2017 homessolutionbd. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var currentTemplabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel! 
    @IBOutlet weak var currentWeatherImage: UIImageView!
    @IBOutlet weak var currentWeatherTypeLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var uitext: UILabel!
    
    var currentWeather : CurrentWeather!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        currentWeather = CurrentWeather()
        currentWeather.downloadWeatherDetails {
            self.UpdateUI()
            print("UI: \(UpdateUI())")
        }
        
        //print(CURRENT_WEATHER_URL)
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath)
        return cell
        
    }
    
    
    func UpdateUI(){
        uitext.text = ("\(currentWeather.CurrentTemp)")
      //  dateLabel.text = currentWeather.Date
       // currentTemplabel.text = ("\(currentWeather.CurrentTemp)")
      
        
//        dateLabel.text = currentWeather.Date
//        currentTemplabel.text = "\(currentWeather.CurrentTemp)"
//        locationLabel.text = currentWeather.CityName
//        currentWeatherTypeLabel.text = currentWeather.WeatherType
       // currentWeatherImages.image = UIImage(named: currentWeather.WeatherType)
    }
    
    
//    func showActivityIndicator(uiview: UIView){
//
//        var aId: UIActivityIndicatorView = UIActivityIndicatorView()
//        aId.frame = CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0)
//        aId.center = uiview.center
//        aId.hidesWhenStopped = true
//        aId.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
//        uiview.addSubview(aId)
//        showActivityIndicator(uiview: UIView)
//    }
}

